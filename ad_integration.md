## Integration von Linux-Clients in eine AD

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111471062793300466</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/linux-ad-integration</span>

> **tl/dr;** _(ca. 9 min Lesezeit): Wenn Linux-Clients in Windows-Netzwerke integriert werden sollen, muss die Authentifizierung und das Freigabemanagement von Linux aus angesprochen werden. Dieser Artikel ist mehr ein Erfahrungsbericht als eine Schritt für Schritt Anleitung, wie ein Linux-Client (Debian-basisiert) in ein Active-Directory eingebunden werden kann: Anpassung der SSSD-Konfigurationsdateien zur Authentifizierung, Anpassung der PAM-Konfigurationsdatein zum Einbinden der Windows-Freigaben und Home-Verzeichnisse._

### Ausgangslage

In vielen Netzwerken ist ein Windows-Server für die Verwaltung der Benutzerkonten zuständig (über _Active Directory_, AD). Windows-Clients sind in die AD-Domäne integriert, authentifizieren sich gegen das AD des Servers und beziehen Freigaben vom Windows-Server - vor allem auch das jeweilige Home-Verzeichnis des Users (unter Windows gelegentlich als "Laufwerk" bezeichnet). In ein solches Netzwerk lassen sich auch Clients mit anderen Betriebssystemen einbinden. Dabei kann die Integration vom manuellen Einhängen von freigegebenen Verzeichnissen bis hin zur Authentifizierung gehen.

In diesem Artikel wird ein Weg dargestellt, wie an einem debian-basierten Linux Client (Ubuntu, MINT,...)

- die Authentifizierung von Windows-Nutzern eines Windows-Server-Active-Directories eingerichtet werden kann,

- automatisch Home-Verzeichnisse für AD-Nutzer eingerichtet werden und 

- die Windows-Freigaben und die Windows-Home-Verzeichnisse für den Nutzer automatisch bei Anmeldung geöffnet werden.

### Vorbereitung

Zunächst sollte das Grundsystem installiert werden, ich habe diese Schritte mit Ubuntu und Linux MINT (unter minimalen Anpassungen) durchgeführt. Am Ende der Basisinstallation sollte auch der Netzwerkzugriff eingerichtet sein und getestet werden. 

Anschließend muss noch dafür gesorgt werden, dass der aktuelle Benutzer `sudo`-Befehle mit `root`-Rechten ausführen darf.

Wenn Du nach der Basiskonfiguration nicht ins Netz kommst, dann nutzt Dein Netzwerk vielleicht einen Proxy, um Datenverkehr zu managen. In meinem Fall ist der Proxyserver unseres Netzwerks auf der IP `10.1.1.10` am Port `8085` erreichbar. Ich muss also vorab dem Debian-Paketmanager `apt` und dem Systems diese Proxy-Informationen zukommen lassen. Das ist bei den meisten Netzewerken vermutlich nicht nötig - und wenn, dann mit anderer IP und anderem Port.

```bash
sudo echo "http_proxy=http://10.1.1.10:8085/\nhttps_proxy=http://10.1.1.10:8085/" | tee -a /etc/environment
sudo echo "Acquire::http::Proxy "http://10.1.1.10:8085/";\nAcquire::https::Proxy "http://10.1.1.10:8085/"; | tee -a /etc/apt/apt.conf.d/proxy.conf
```

Am Ende der Basiskonfiguration ist es eine gute Idee, das System auf den aktuellen Stand zu bringen (und damit gleich zu probieren, ob soweit alles klappt):

```bash
sudo apt-get update
sudo apt-get upgrade -y
```

Die folgenden Schritte führt man natürlich am besten in einem Skript aus - v.a. wenn es sich um mehrere Clients handelt.

### Den Client in die Domäne aufnehmen

#### Abhängigkeiten laden

Zu Beginn werden einige wesentliche Abhängigkeiten geladen. Ich beginne immer mit dem Tool, das den Client in die Windows-Domäne aufnimmt (`realmd`) und einigen Basiskomponenten für die Authentifikation. Python sollte für einige der Skripte ebenfalls vorhanden sein. Es geht um die folgenden Pakete:

|Name|Beschreibung|
|---|---|
|`realmd`|Tool, um sich einer Activy-Directory-Domäne (oder anderen Kerberos Realms) anzuschließen|
|`adcli`|Ein Kommandozeilen-Client für eine Active Directory (ggf. für Tests nützlich)|
|`sssd`<br/>` sssd-tools`<br>`libsss-simpleifp0`|Der _System Security Services Daemon_ stellt Dienste zur Verfügung, die die Authentifizierung gegen externe Autoritäten ermöglichen und sich auch um den Zugriff auf Netzwerkfreigaben kümmern.|
|`python2`<br/>` python3`|Der Python-Interpreter in der alten (aber in alten Skripten noch gebräuchlichen) und neuen Version. Wird später ohnehin gebraucht.|

Diese können in einem Rutsch installiert werden mit dem folgenden Befehl. Sollte der Befehl in einem Skript ausgeführt werden, sollten noch die Optionen `-y -q` ergänzt werden,damit bei allen Nachfragen die Standard-Option ausgewählt wird und es keine Ausgabedaten gibt.

```bash
sudo apt-get install adcli realmd sssd-tools libsss-simpleifp0 python3 python2
```

#### Die eigentliche Aufnahme in die Domäne

Wenn alle Abhängigkeiten geladen sind, kann man zunächst einmal ausprobieren, ob der Client die _Active-Directory_-Domäne überhaupt findet. Als Domänenbezeichnung nutze ich hier `musterdomain.local`: das muss natürlich jeweils ersetzt werden. Im folgenden Befehl kann die Domänenbezeichnung auch weggelassen werden, dann gibt er Informationen zu allen AD-Domänen aus, die er findet.

```bash
realm discover musterdomain.local
```

Jetzt wird es ernst: Der Client soll der Domäne beitreten. Dazu braucht man die Zugangsdaten des Accounts, der in dem AD die Berechtigung hat, neue Clients aufzunehmen (i.d.R. `Administrator` oder `admin` - manchmal auch speziell angelegte Nutzer):

```bash
sudo realm join musterdomain.local --user=Administrator
```

Es wird nach den Zugangsdaten gefragt (Vorsicht: zuerst das `sudo`-Passwort, dann das Passwort des gewählten AD-Kontos).

Falls es hier Probleme gibt: beim Aufnehmen des Clients lohnt ein Blick auf die Systemuhr: Linux nimmt an, dass die Hardwareuhr in UTC läuft, wohingegen Windows die Hardwareuhr häufig auf Lokalzeit stellt. Das kann zu Problemen führen bei der Authentifizierung mit der AD! (Bitte den Punkt schonmal merken: das war bei mir auch später eine häufige Fehlerursache.)

Bis hierhin ergibt es Sinn, alles händisch an jedem Client zu machen oder gesondertes Skript auszuführen, da die Passworteingabe ohnehin erfolgen muss. Wenn es mehrere Clients sind, wäre jetzt ein guter Punkt, sich mit `ansible` zum Verteilen von Konfigurationen und Befehlen zu beschäftigen. Damit wird dann alles nur einmal als _Playbook_ zusammengefasst, dass an allen angemeldeten Clients auf einen Schlag ausgeführt wird. Auf Ansible gehe ich in einem späteren Blogpost ein.

Zum Verständnis des Authentifizierungsprozesses stelle ich hier einmal die einzelnen Schritte dar, die jetzt folgen müssen, damit Authentifizierung und Freigaben funktionieren.

### Erforderliche Pakete für Authentifizierung und Freigabe-Einbindung

Zu Beginn des nächsten Schrittes empfehle ich, einige neue Abhängigkeiten zu installieren. Wir brauchen nicht alle auf einmal, aber so vergessen wir nichts. Bei der _Schritt für Schritt_-Konfiguration kann es sein, dass Abhängigkeiten nicht gefunden werden, wenn zuvor nicht alles installiert wurde (hatte ich so nicht ausprobiert).^[Als Grundlage dieser Schritte hat mir eine Anleitung unter https://kb.iu.edu/d/armi gedient, die jetzt leider nicht mehr online ist] Kurz eine Übersicht: 

|Name|Beschreibung|
|---|---|
|`cifs-utils`|Um SMB-Freigaben direkt mit `mount` einbinden zu können, ist dieses Paket notwendig|
|`libpam-mount`|Ermöglicht es, Freigaben bei Anmeldung bestimmter nutze (mit den Rechten der Nutzer) einzubinden (zu mounten)|
|`oddjob-mkhomedir`|Für AD-Nutzer, die sich noch nie an dem jeweiligen Client angemeldet hatten, <br/>gibt es noch kein Home-Verzeichnis. Dieses Paket erstellt eins.|
|`keyutils`|Tool, um Schlüssel (also Zugangsdaten) im Kernel zu verwalten|
|`krb5-user`|Basiskomponenten für den von der AD genutzten Netzwerk-Authentifizierungsdienst Kerberos|
|`krb5-config`|Konfiguration für Kerberos|
|`sssd-krb5`|Die Konfigurationsdatei, um Kerberos mit SSSD (siehe ganz oben) zu nutzen.|
|`libpam-krb5`|Einbindung von Kerberos in die "Pluggable Authentication Modules" (PAM)|

Zusätzlich installiere ich in diesem Zusammenhang noch eine Reihe von Tools, die ich i.d.R. in dieser Phase benötige:

|Name|Beschreibung|
|---|---|
|`openssh-server`<br/>`openssh-client`|SSH-Client/Server, um später auf die anderen Clients zugreifen zu können|
|`samba`<br/>`smbclient`|Zugriff auf die SMB-Freigaben von Windows-Servern. <br/>`smbclient` stellt eine Konsolenapp dafür zur Verfügung.|
|`hxtools`|Toolsammlung|
|`chrony`|Client, der die Zeitsynchronisierung über das Netzwerk (NTP) ermöglicht.|

Wer alles auf einmal installieren will, im Ganzen lautet der Installationsbefehl:

```bash
sudo apt-get install oddjob-mkhomedir openssh-server openssh-client keyutils libpam-mount cifs-utils krb5-config sssd-krb5 libpam-krb5 krb5-user smbclient hxtools samba chrony
```

### Konfiguration des _System Security Services Daemon_ (sssd in `/etc/sssd/sssd.conf`)

Die Konfiguration betrifft die bereits installierten Komponenten:

```bash
sudo apt-get install sssd sssd-tools  libsss-simpleifp0 sssd-krb5
```

Damit die Authentifizierung gegen das AD klappt, muss zunächst an relativ vielen Stellen in der `sssd.conf` die Domäne eingetragen werden. (Ich hatte sie an einer Stelle in Großbuchstaben eingetragen und hab das mal hier so wieder gegeben - weiß aber nicht, ob das damals wirklich nötig war).

Wichtig hier ist u.a., an welchem Ort die Home-Verzeichnisse der AD-User angelegt werden. Es gibt im Wesentlichen drei denkbare Strategien: 

- AD-User wie lokale User direkt unter `/home` (`/home/%u`), 

- AD-Nutzer direkt unter `/home`, jedoch dem Verzeichnisnamen die Domain angehängt (`/home/%u@%d`) oder 

- AD-Nutzer in einem gesonderten Unterordner, der den Namen der AD trägt (`/home/%d/%u`). Bei letzterem hatte ich bei Ubuntu Probleme mit _Snap_-Paketen, die aus mir unerfindlichen Gründen damit nicht klargekommen sind.

Die Datei `/etc/sssd/sssd.conf` sieht in meinem Beispiel so aus (Auszug):

```INI
[sssd]
domains = musterdomain.local
config_file_version = 2
services = nss, pam, ssh

[domain/domains = musterdomain.local]
ad_domain = domains = musterdomain.local
krb5_realm = domains = MUSTERDOMAIN.LOCAL
realmd_tags = manages-system joined-with-adcli 
cache_credentials = True
id_provider = ad
auth_provider = ad
chpass_provider = ad
access_provider = ad
krb5_store_password_if_offline = True
krb5_ccname_template = FILE:/%d/krb5cc_%U
default_shell = /bin/bash
#ldap_id_mapping = True
# 'True' requires 'username@iu.edu' to log in, 'False' requires 'username' to log in
use_fully_qualified_names = False
fallback_homedir = /home/%u@%d
#fallback_homedir = /home/%u
# Place all ADS user accounts in the same directory 
# override_homedir = /home/%d/%u
# Place all (local + AD) user accounts in the same directory 
override_homedir = /home/%u
#create_homedir = true
#remove_homedir = true
access_provider = ad
```

### Konfiguration des Kerberos-Authentifizierungsdienstes (`/etc/krb5.conf`)

Die zugehörigen Pakete (falls noch nicht installiert) wären:

```bash
sudo apt-get install krb5-user krb5-config sssd-krb5 libpam-krb5
```

Diese Konfiguration geht relativ schnell: in der Datei `/etc/krb5.conf` muss lediglich die Domaine (mehrfach) eingetragen werden:

```INI
[realms]
	DEINEDOMANE = {
		kdc = musterdomain.local
		admin_server = musterdomain.local
		default_domain = musterdomain.local
	}
```

### Die Konfiguration der Einhängepunkte für Netzwerkfreigaben

Wenn vom Server SMB-Netzwerkfreigaben eingehängt werden sollen, damit auf gemeinsame Daten (und die Windows-Homeverzeichnisse) zugegriffen werden kann, müssen die entsprechenden Pakete installiert und konfiguriert sowie eine Template-Verzeichnisstruktur angelegt werden. 

Das betrifft die folgenden Pakete:

```bash
sudo apt-get install cifs-utils libpam-mount oddjob-mkhomedir samba smbclient
```

Die Freigaben werden mit den Berechtigungen des jeweiligen Users eingehängt (gemountet), theoretisch können zeitgleich unterschiedliche User am Client angemeldet sein. Es spricht also viel dafür, die Verzeichnisse nicht unter `/mnt/` (wie gelegentlich bei NFS-Freigaben oder Wechselmedien üblich), sondern im jeweiligen Userverzeichnis einzuhängen.

Eine weitere Entscheidung, die getroffen werden muss: sollen die User unter Linux das gleiche Home-Verzeichnis nutzen, dass sie auch unter Windows nutzen? Also sollen sie in den gleichen Downloads-Ordner schreiben, den gleichen Desktop-Ordner nutzen - kurz das Windows-Homeverzeichnis direkt unter `/home/$username` einbinden? Ich hatte mich dagegen entschieden - das klingt nach Problemen... wer das trotzdem versuchen will: Ich bin über jeden Erfahrungsbericht dankbar! 

Ich binde die Windows-Home-Verzeichnisse unter `/home/$username/serverhome` ein. Einzelne Ordner können später per _Symlink_ auf die Windows-Ordner verweisen (z.B. der Desktop oder Downloads-Ordner). Der gravierende Nachteil dieser Variante: die Daten im Linux-Userordner liegen lokal auf den Clients (bzw. müssen gesondert, z.B. über NFS auf einem Server gehalten werden). Nur Daten, die im Windows-Homeverzeichnis unter `/home/$username/serverhome` liegen, lassen sich dann auch unter Windows-Anmeldungen finden. Das lernen die Nutzer aber schnell und schmerzvoll beim ersten mal Suchen nach nicht synchronisierten Daten...

Damit alle User über diese Einhängepunkte verfügen (im Userordner `~/serverhome`), bietet sich an, diese in einer Template-Struktur für neue Userordner anzulegen. Linux nutzt hierfür häufig das Verzeichnis `/etc/skel`: hier befindet sich das Gerüst (_skeleton_) eines neuen Userverzeichnisses. In diesem Beispiel werden zwei Verzeichnisse angelegt: eines für das Windows-Home (`~/serverhome`) und eines für eine Team-Freigabe (`~/tausch`) - das kann natürlich beliebig angepasst werden:

```bash
mkdir -p /etc/skel/tausch
mkdir -p /etc/skel/serverhome
```

#### Userverzeichnisse mit Einhängepunkten nutzen: `/etc/pam.d/common-session`

Damit neue AD-Nutzer das obige Home-Verzeichnis-Gerüst unter `/etc/skel` auch erhalten, muss dieses noch in der `/etc/pam.d/common-session` eingetragen werden. Neben dem Pfad werden noch die Benutzerrechte festgelegt (`umask=0077` entzieht allen bis auf dem Eigentümer Lese-, Schreib- und Ausführungsrechte).

Direkt nach der Zeile `session required pam_unix.so` muss `session required pam_mkhomedir.so skel=/etc/skel/ umask=0077` eingefügt werden (hier die fünftunterste Zeile). Bei mir sieht das dann so aus: 

```bash
#...
# and here are more per-package modules (the "Additional" block)
session	required	pam_unix.so 
session required  pam_mkhomedir.so skel=/etc/skel/ umask=0077
session	optional	pam_sss.so 
session	optional	pam_mount.so disable_interactive
session	optional	pam_systemd.so 
# end of pam-auth-update config
```

#### Anpassungen in der Authenfizierungs-Konfiguration der `/etc/pam.d/common-auth`

In der Datei `common-auth` muss die Zeile `auth	optional	pam_mount.so` eingefügt werden. Ich habe sie später mit der Option `disable_interactive` ergänzen müssen, da `pam_mount` nach einem Update angefangen hatte, ständig nach Passwörtern zu fragen (den Lösungshinweis hatte ich [hier gefunden](https://unix.stackexchange.com/questions/120762/why-does-pam-mount-ask-for-password)):

```bash
#...
# and here are more per-package modules (the "Additional" block)
auth	optional	pam_mount.so disable_interactive
auth	required	pam_ecryptfs.so unwrap
auth	optional	pam_cap.so 
# end of pam-auth-update config
```

##### Einhängen der Netzwerk-Freigaben (Konfiguration der `/etc/security/pam_mount.conf.xml`)

Das Herzstück (und die häufigste Fehlerquelle) beim Einhängen der Windows-Freigaben ist die Datei `pam_mount.conf.xml`. Hier muss im XML-Element  `<volume ...>` relativ viel individuell angepasst werden (und das Element ggf. häufiger eingesetzt werden):

- `server`: Der FQDN des Servers, der die Freigabe zur Verfügung stellt (i.d.R. der Windows-Server selbst)
- `path`: der Pfad, mit dem die jeweilige Freigabe auf dem obigen Server angesprochen wird (ohne führenden Slash)
- `mountpoint`: Nach der Logik der Benutzerordner (siehe oben) und Verzeichnisstruktur unter `skel` muss hier der korrekte Einhängepunkt genannt werden.

Als einfacher Test: Wenn der Benutzer `max.mustermann` sein Windows-Homelaufwerk am Windows-Client im Exporer unter 

```
\\server.musterdomain.local\Benutzer\max.mustermann
```

erreicht, dann ist `server.musterdomain.local` der Servername und `Benutzer/%(DOMAIN_USER)` (Achtung: Backslash wird zu Slash!) ist der Pfad:

```xml
	<!-- Volume definitions -->
	<volume user="*" fstype="cifs" server="server.musterdomain.local" 
    path="Benutzer/%(DOMAIN_USER)"  
    mountpoint="/home/%(DOMAIN_USER)/serverhome"  
    options="sec=krb5,cruid=%(USERUID)" />
		
<volume user="*" fstype="cifs" server="server.musterdomain.local" 
    path="Tausch" 
    mountpoint="/home/%(DOMAIN_USER)/tausch" 
    options="sec=krb5,cruid=%(USERUID)" />
```

#### Kerberos Session-Keys erhalten (Konfiguration in `/etc/request-key.d/cifs.spnego.conf`)

Damit die Kerberos-Sessions erfolgreich aufgebaut werden können, ist noch folgenden Konfiguration nötig:

```
create  cifs.spnego    * * /usr/sbin/cifs.upcall -t %k
```


### Anmeldung mit User & Login (statt vorausgewählter User)

Bei einigen Systemen gibt es das Problem, dass der jeweilige Desktop-Manager beim Anmeldescreen nur die lokalen Nutzer anbietet - man hat dann gar nicht die Wahl, sich mit den AD-Nutzern anzumelden.

Hierfür ist die Konfiguration nötig, die Anmeldung mit Login/Passwort ermöglicht. Im Fall von Gnome geht das z.B., in dem die Datei `/etc/gdm3/greeter.dconf-defaults` angepasst wird:^[siehe [https://wiki.ubuntuusers.de/GDM/](https://wiki.ubuntuusers.de/GDM/)]

```ini
[org/gnome/login-screen]
disable-user-list=true 
```

Bei anderen Display-Managern muss das auf anderem Weg erfolgen.


### Testen der Konfiguration

Der beste Test ist natürlich, hier den Rechner einmal neu zu starten. Wenn die Anmeldung mit einem AD-Konto klappt - wunderbar. Erscheinen die Freigabeverzeichnisse? Falls nicht, wäre ein Weg zunächst zu schauen, ob die Authentifizierungsdaten (das AD-Ticket) vorhanden sind:

```
klist
```

und dann die manuelle Verbindung zu probieren:

```
smbclient -k -L //server.musterdomain.local
```

### Nächste Schritte

Ich habe diesen Artikel auf Basis meiner Notizen zusammengestellt. Da ich direkt nach der Integration in die Domäne (`realm`) die einzelnen Befehle nur noch über _Ansible-Playbooks_ ausführe, kann es sein, dass sie auf aktuellen System etwas anders funktionieren. Ich bin für jeden Hinweis dankbar, dann pflege ich das ein. In einem folgenden Artikel werde ich das Konfigurationsmanagement für mehrere Linux-Clients mit Ansible beschreiben.
